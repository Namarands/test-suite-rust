use std::fs::File;
use std::io::prelude::*;
use serde_json;

#[derive(Debug, Serialize, Deserialize)]
struct Test {
    name : String,
    files : Vec<String>,
    flags : Vec<String>,
    #[serde(default)]
    valgrind : bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TestList {
    path : String,
    tests : Vec<Test>
}

pub fn read_file(filename : &str) -> TestList {
    let mut file = File::open(filename).expect(
        format!("Error: file not found: {}", filename).as_str());
    let mut content = String::new();
    file.read_to_string(&mut content).expect(
        format!("Error: cannot read file: {}", filename).as_str());
    serde_json::from_str(content.as_str()).expect(
        format!("Error: invalid json file: {}", filename).as_str())
}