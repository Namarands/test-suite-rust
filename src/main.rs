extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate term_painter;
extern crate git2;

mod cloner;
mod executor;
mod reader;

use term_painter::ToStyle;
use term_painter::Color::{Yellow, Red};

pub fn warning(s : &str) {
    println!("{} {}", Yellow.paint("WARNING:"), s);
}

pub fn error(s : &str) {
    println!("{} {}", Red.paint("ERROR:"), s);
}

fn main() {
    let b = reader::read_file("C:/Users/Namarand/Documents/rust_suite/test/test.json");
    println!("{:?}", b);
}
