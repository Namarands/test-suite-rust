use 

pub create_ssh(pass : &str, ssh : &str, ssh_pub : &str) -> Result<Cred, ()> { 
    match Cred::ssh_key("git", Some(Path::new(ssh_pub)), Path::new(ssh), Some(pass)) {
        Ok(cred) => Ok(cred),
        Err => {
            error("failed to create git identity");
            Err(())
        }
    }
}

pub fn clone(repo : &str, path : &str, cred : &Cred) -> Result<(),()> {
    let mut builder = RepoBuilder::new();
    builder.clone(repo_url, Path::new(repo_clone_path))
}